<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;
use App\Models\Review;

class ReviewSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
          // Get all posts
          $posts = Post::all();

          // Create more than 20k reviews for each post
          $posts->each(function ($post) {
              $post->reviews()->saveMany(Review::factory()->count(rand(1, 10))->make());
          });
    }
}
