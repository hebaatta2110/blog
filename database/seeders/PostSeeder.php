<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Post;

use Database\Factories\PostFactory;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
         // Create 50k posts
         Post::factory()->count(50000)->create();
    }
}
