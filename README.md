
# Project

composer install

cp .env.example .env

php artisan key:generate

Set up your database connection in the .env file

php artisan migrate

php artisan serve

## URL for published documentation
https://documenter.getpostman.com/view/8479337/2sA3BheEbb
