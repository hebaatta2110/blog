<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;


class PostController extends Controller
{
    //
    public function AddPost(Request $request)
    {
        $validatedData = $request->validate([
            'title' => 'required|string|max:255',
            'body' => 'required|string',
        ]);

        // Create a new post using the validated data
        $post = Post::create([
            'title' => $validatedData['title'],
            'body' => $validatedData['body'],
            'user_id' => 1, // Assuming authentication is implemented and user ID is available
        ]);
        return response()->json([
        'message' => 'Post created successfully', 
        'post' => $post], 201);

    }

    public function listUserPosts(Request $request)
    {
        //$userId = $request->user()->id; // Assuming you're using Laravel's authentication
        $userId = 1;
        $posts = Post::where('user_id', $userId)->paginate(10); // Paginate the results, with 10 posts per page

        return response()->json($posts);
    }

    public function listTopPosts(Request $request)
    {
        $posts = Post::withAvg('reviews', 'rating')
                     ->orderByDesc('reviews_avg_rating')
                     ->paginate(10); // Paginate the results, with 10 posts per page

        return response()->json($posts);
    }
}
