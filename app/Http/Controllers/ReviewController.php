<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Review;

class ReviewController extends Controller
{
    //
    public function addReview(Request $request, $postId)
    {
        // Validate the incoming request data
        $validatedData = $request->validate([
            'rating' => 'required|integer|min:1|max:5',
            'comment' => 'required|string',
        ]);

        // Find the post by ID
        $post = Post::findOrFail($postId);

        // Create a new review for the post using the validated data
        $review = $post->reviews()->create([
            'rating' => $validatedData['rating'],
            'comment' => $validatedData['comment'],
            'user_id' => 1, // Assuming authentication is implemented and user ID is available
        ]);

        // Return a success response with the created review data
        return response()->json(['message' => 'Review added successfully', 'review' => $review], 201);
    }
}
