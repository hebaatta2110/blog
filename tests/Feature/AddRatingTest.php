<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Post;
use App\Models\Review;
use App\Models\User;
class AddRatingTest extends TestCase
{
    public function testUserCanAddRatingToPost()
    {
        // Create a user
        $user = User::factory()->create();

        // Create a post
        $post = Post::factory()->create();

        // Authenticate the user
        $this->actingAs($user);

        // Send a POST request to add a rating to the post
        $response = $this->post("/api/posts/{$post->id}/reviews", [
            'rating' => 4,
            'comment' => 'Great post!',
        ]);

        // Assert that the response indicates success
        $response->assertStatus(201);

        // Assert that a review was added to the post
        $this->assertDatabaseHas('reviews', [
            'post_id' => $post->id,
            'user_id' => 1,
            'rating' => 4,
            'comment' => 'Great post!',
        ]);
    }
   
}
