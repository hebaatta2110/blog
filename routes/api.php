<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\PostController;
use App\Http\Controllers\ReviewController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/addpost',[PostController::class, 'AddPost']);
Route::post('/posts/{post_id}/reviews',[ReviewController::class, 'addReview']);
Route::get('/user/posts', [PostController::class, 'listUserPosts']);
Route::get('/top/posts', [PostController::class, 'listTopPosts']);
